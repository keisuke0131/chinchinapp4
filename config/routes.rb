Rails.application.routes.draw do
  post "follows/:user_id/create" => "follows#create"
  post "follows/:user_id/destroy" => "follows#destroy"

  post "comments/destroy/:id" => "comments#destroy"
  get 'comments/new/:post_id' => "comments#new"
  get "comments/index/:post_id" =>"comments#index"
  post 'comments/create/:post_id' =>"comments#create"
  

  post "likes/:post_id/create" => "likes#create"
  post "likes/:post_id/destroy" => "likes#destroy"

  post "users/:id/update" => "users#update"
  get "users/:id/edit" => "users#edit"
  post "users/create" => "users#create"
  get "signup" => "users#new"
  get "users/index" => "users#index"
  get "users/:id" => "users#show"
  post "login" => "users#login"
  post "logout" => "users#logout"
  get "login" => "users#login_form"
  get "users/:id/likes" => "users#likes"
  get "users/:id/follow" => "users#follow"
  get "users/:id/follower" => "users#follower"
  post "users/destroy" => "users#destroy"

  get "posts/index" => "posts#index"
  get "posts/new" => "posts#new"
  get "posts/:id" => "posts#show"
  post "posts/create" => "posts#create"
  get "posts/:id/edit" => "posts#edit"
  post "posts/:id/update" => "posts#update"
  post "posts/:id/destroy" => "posts#destroy"

  get "/" => "home#top"
  get "about" => "home#about"
end
