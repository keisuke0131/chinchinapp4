class FollowsController < ApplicationController
  def create
    @follow=Follow.new(
      follow_user_id: params[:user_id],
      follwers_user_id:@current_user.id
    )
    if @follow.save
      flash[:notice]="フォローしました"
      redirect_to("/users/#{@follow.follow_user_id}")
    end
  end

  def destroy
    @follow=Follow.find_by(
      follow_user_id: params[:user_id],
      follwers_user_id:@current_user.id
    )
    if @follow.destroy
      flash[:notice]="フォロー解除しました"
      redirect_to("/users/#{@follow.follow_user_id}")
    end
  end

end
