class CommentsController < ApplicationController
  def new
    @comment=Comment.new
    @post=Post.find_by(id: params[:post_id])
  end

  def create
    @comment=Comment.new(
      post_id: params[:post_id],
      user_id: @current_user.id,
      content: params[:content]
    )
    if @comment.save
      flash[:notice] = "コメントを作成しました"
      redirect_to("/comments/index/#{@comment.post_id}")
    else
      render("comments/new")
    end
  end

  def index
    @comments=Comment.where(post_id: params[:post_id]).order(created_at: :asc)
  end

  def destroy
    @comment=Comment.find_by(id: params[:id])
    @post=Post.find_by(id: @comment.post_id)
    @comment.destroy
    flash[:notice]="コメントを削除しました"
    redirect_to("/comments/index/#{@post.id}")
  end
end
