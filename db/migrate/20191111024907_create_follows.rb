class CreateFollows < ActiveRecord::Migration[5.2]
  def change
    create_table :follows do |t|
      t.integer :follow_user_id
      t.integer :follwers_user_id

      t.timestamps
    end
  end
end
